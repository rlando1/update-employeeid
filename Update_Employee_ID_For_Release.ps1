﻿# 
# This script will add the EmployeeID attribute from a database to on-prem AD
# This version will ignore them if they have an existing employeeID (the other version does not)
#
# By: Landon Lengyel (landon@almonde.org)
# Version: 2021-03-25
#

#############################
# Variables
#############################
# Export path for logging info (csv)
$CsvLogPath = ".\EmployeeIDLog.csv"
# Delimiter of log CSV, pipe is strongly recommended "|"
$CsvDelimiter = "|"
# AD servers to pull from. Parent and/or child domains
$ADServers = @('subdomain.example.com','example.com')
# Dry run, mark yes to not commit any changes to Active Directory
$DryRun = $False

## --- Database variables ---
# DB credentials stored in seperate file so you can .gitignore
# DB credentials should be in the same directory
# Example for json file in README
$DBCredsFileName = "DB_Creds.json"
$SQLDBSizeQuery = "SELECT [FirstName], 
    [LastName],
    [PreferredName],
    [EmployeeID]
FROM [Employee_view.vw]"
#WHERE [Status]= 'Active'
#    AND [Deleted]= 0"

## --- Column attribute matching ($ADAttribute = "Database Column Name") --- 
$givenName = "FirstName"
$preferredName = "PreferredName" # an alternative first name
$sn = "LastName"    # surname, called 'sn' in AD
$EmployeeID = "EmployeeID"


#############################
# Functions
#############################

## Add-Log is a function to ensure consistency of logs, as well as allowing future additions to the log without updating the underlying code of the script
## Depends on $CsvLogPath global variable
## Does not return anything
function Add-Log {
    param (
        # Log Type allows for easy filtering. (Ex. Info, Error, FatalError, Change, etc)
        [Parameter(Mandatory=$True)]
        [string]$LogType,

        # Details for the specifics on what is being logged
        [Parameter(Mandatory=$True)]
        [string]$Details,

        # Database Employee ID for relevant employee. Not always necessary for general 'Info' logging
        [Parameter(Mandatory=$False)]
        [string]$DBEmployeeID = "",

        # AD ObjectGUID for relevant Active Directory User. Not always necessary
        [Parameter(Mandatory=$False)]
        [string]$ADObjectGUID = ""
    )

    # Date Time is in ISO 8601 format
    $DateTime = Get-Date -Format "yyyy-MM-ddTHH:mm:ss"

    # When editing, also edit the "Creating log file" code if needed
    # Columns: Date Time, Log Type, Database EmployeeID, AD ObjectGUID, Details
    Add-Content -Path $CsvLogPath -Value ($DateTime + $CsvDelimiter + $LogType + $CsvDelimiter + $DBEmployeeID + $CsvDelimiter + $ADObjectGUID + $CsvDelimiter + $Details)

}

# TODO: Email notification function for errors

#############################
# Main
#############################

Clear-Host

## Creating log file
if (Test-Path $CsvLogPath) {
    Write-Host "Log file already exists. Continuing to add data to end of the file"
}
else {
    try { 
        New-Item $CsvLogPath
        Add-Content -Path $CsvLogPath -Value ("Timestamp" + $CsvDelimiter + "Log Type" + $CsvDelimiter + "DB EmployeeID" + $CsvDelimiter + "AD ObjectGUID" + $CsvDelimiter +  "Details")
    }
    catch {
        # Writing in multiple streams in an attempt to notify an admin since the log file is unavailable
        Write-Host "Error creating and/or writing to log file. Exiting..."
        Write-Error "Error creating and/or writing to log file. Exiting..."
        exit
    }
}


# Parse sql credentials from json
$ScriptDir = Split-Path $script:MyInvocation.MyCommand.Path
$DBCreds = Get-Content ($ScriptDir + '\' + $DBCredsFileName) | ConvertFrom-Json

$RemoteSQLInstance = $DBCreds[0].RemoteSQLInstance
$RemoteSQLDatabaseName = $DBCreds[0].RemoteSQLDatabaseName
$RemoteSQLUser = $DBCreds[0].RemoteSQLUser
$RemoteSQLPassword = $DBCreds[0].RemoteSQLPassword

# Verify SQL credentials were parsed
if ($null -eq $RemoteSQLInstance -or $null -eq $RemoteSQLDatabaseName -or $null -eq $RemoteSQLUser){
    Write-Host -ForegroundColor Red "Database credentials not parsed from json. Exiting"
    Log-Add -LogType "FatalError" -Details "Database credentials not parsed from json. Exiting"
}


# Get the SqlServer module if needed
$SQLModuleCheck = Get-Module -ListAvailable SqlServer
if ($null -eq $SQLModuleCheck) {
    write-host "SqlServer Module Not Found - Installing"
    # Not installed, trusting PS Gallery to remove prompt on install
    Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
    # Installing module
    Install-Module -Name SqlServer –Scope CurrentUser -Confirm:$false -AllowClobber
}
Import-Module SqlServer


Add-Log -LogType "Info" -Details "Beginning script"

# Getting data from Database
$DatabaseData = Invoke-SqlCmd -Query $SQLDBSizeQuery -ServerInstance $RemoteSQLInstance -Database $RemoteSQLDatabaseName -Username $RemoteSQLUser -Password $RemoteSQLPassword
if ($null -eq $DatabaseData -or $DatabaseData.Count -eq 0) {
    Add-Log -LogType "FatalError" -Details "Failed to retrieve data from database. Exiting"
}


foreach ($DatabaseUser in $DatabaseData) {
    # Clarify name attributes
    $firstName = $DatabaseUser."$givenName"
    $preferredName = $DatabaseUser."$preferredName"
    $lastName = $DatabaseUser."$sn"
    $currentEmployeeID = $DatabaseUser."$EmployeeID"
    
    Write-Host "Checking: $firstName $lastName -- $currentEmployeeID"

    # Start with $null to check for lack of matches, and prevent old data leaking into loop
    $ADUser = $null

    ## Attempt to find user by EmployeeID
    # Some properties don't exist in parent domain vs child domain so selecting "*" properties will throw an error
    $ADUser = @()
    foreach ($ADServer in $ADServers) {
        $ADUser += Get-ADUser -Filter "EmployeeID -eq '$currentEmployeeID'" -Server $ADServer -Properties name,SamAccountName,EmployeeID,CanonicalName,objectGUID | Select name, SamAccountName, EmployeeID, CanonicalName, objectGUID
        # No need to search the second server if it already found a user here.
        # This cannot be done below, because it needs to verify that it's searched for all users with the same name in all domains
        if ($ADUser.Count -ne 0) { break }
    }


    ## User not found by EmployeeID, attempt to match names
    if ($ADUser.Count -eq 0){

        ## Break apart names so we can perform different combinations of matches below. Also consider preferredname a possibility for firstName
        ## For example:
        ## firstName: "John-Philip"
        ## lastName: "Smith Price"
        ## Would be tested as "John Smith", "John Price", "Philip Smith", and "Philip Price"

        # Arrays to kep track of name combinations 
        $firstNameArray = [System.Collections.ArrayList]::new()
        $lastNameArray = [System.Collections.ArrayList]::new()

        # Add existing names as they exist in database. Ex: "John-Philip" and "Smith Price"
        # Routes to $null to supress the addition output
        $firstNameArray.Add($firstName) > $null
        $lastNameArray.Add($lastName) > $null

        # Split last name by hyphen if necessary
        if ($lastName -match '-'){
            $lastName = $lastName.split('-')

            # Add lastname combinations
            foreach ($IndivLastName in $lastName) {
                $lastNameArray.Add($IndivLastName) > $null
            }
        }
        # Split last name by space if necessary
        if ($lastName -match ' '){
            $lastName = $lastName.split(' ')

            # Add lastname combinations
            foreach ($IndivLastName in $lastName) {
                $lastNameArray.Add($IndivLastName) > $null
            }
        }


        # Split first name by hyphen if necessary
        if ($firstName -match '-') {
            $firstName = $firstName.split('-')

            # Add firstName possible combinations
            foreach ($IndivFirstName in $firstName) {
                $firstNameArray.Add($IndivFirstName) > $null
            }
        }
        # Split first name by space if necessary
        if ($firstName -match ' '){
            $firstName = $firstName.split(' ')

            # Add firstName possible combinations
            foreach ($IndivFirstName in $firstName) {
                $firstNameArray.Add($IndivFirstName) > $null
            }
        }

        # Check for preferredName, and perform the same actions if it exists
        # Uses regex to test for empty whitespace
        if ($preferredName -ne $null -and $preferredName -ne "NULL" -and $preferredName -notmatch '^\s+$' -and $preferredName -ne '') {
            # Split first name by hyphen if necessary
            if ($preferredName -match '-') {
                $preferredName = $preferredName.split('-')
            }
            # Split first name by space if necessary
            if ($preferredName -match ' '){
                $preferredName = $preferredName.split(' ')
            }

            # preferredName is a replacement first name, so combine those arrays
            foreach ($IndivPreferredName in $preferredName) {
                $firstNameArray.Add($IndivPreferredName) > $null
            }
        }

        # Create new arraylist to condition for multiple matches
        $MatchingADUsers = [System.Collections.ArrayList]::new()

        ## Here we will attempt combinations of the firstnames and lastnames to find a match in AD. 
        # If the user does not have multiple combinations (Ex. no spaces, hyphens, etc) in their name, it will only have one combo
        foreach ($IndivFirstName in $firstNameArray) {
            foreach ($IndivLastName in $lastNameArray) {
                # make TempUser equal $null to check for lack of matches, and prevent old data leaking into loop
                # Some properties don't exist in parent domain vs child domain so selecting "*" properties will throw an error
                $TempUsers = @()
                foreach ($ADServer in $ADServers) {
                    $TempUsers += Get-ADuser -Filter { (givenName -like $IndivFirstName) -and (sn -like $IndivLastName) } -Server $ADServer -Properties name,SamAccountName,EmployeeID,CanonicalName,objectGUID | Select name, SamAccountName, EmployeeID, CanonicalName, objectGUID
                }

                # Add to arraylist if it returned a result
                # If there are multiple returned users in one query (ie, mulitple 'John Doe') add each of them to the arraylist as individual entries
                if ($TempUsers -is [array]) {
                    foreach ($TempUser in $TempUsers) {
                        $MatchingADUsers.Add($TempUser) > $null
                    }
                }
                elseif ($TempUsers -ne $null) {
                    $MatchingADUsers.Add($TempUsers) > $null
                }
                 
            }
        }

        ## Here we are going to decide to make changes, or not based on the matches found above

        # Check if matches already have conflicting employeeID's, if so, remove them
        # Cannot modify the arraylist while it's being looped
        $itemsToRemove = @()
        if ($MatchingADUsers.Count -ge 1) {
            foreach ($MatchingADUser in $MatchingADUsers) {
                if ($MatchingADUser.EmployeeID -ne $null) {
                    $itemsToRemove += $MatchingADUser
                }
            }
        }
        foreach ($item in $itemsToRemove) {
            $MatchingADUsers.Remove($item)
        }


        # If there are no matches in AD
        if ($MatchingADUsers.Count -eq 0) {
            Write-Host -ForegroundColor Red "No match for employee found in Active Directory"
            Add-Log -LogType "Error" -DBEmployeeID $currentEmployeeID -Details "Cannot find matching AD user account for the employee database"
        }
        # If there is a singular match in AD, then commit the change
        elseif ($MatchingADUsers.Count -eq 1) {
            Write-Host -ForegroundColor Green "Single match for employee in Active Directory. Adding employeeID"
            Add-Log -LogType "Change" -DBEmployeeID $currentEmployeeID -ADObjectGUID $MatchingADUsers[0].objectGUID -Details "Adding missing employeeID in AD to user account"
            
            # Only commit the change if this is not a dry run
            if ($DryRun -eq $False) {
                # Attempt set-aduser for each provided AD Server
                # If it throws an error, it will try the next server
                foreach ($ADServer in $ADServers) {
                    # Set-ADUser will convert $SetError to an ArrayList, regardless of success
                    $SetError = $null
                    Set-ADuser -Server $ADServer -Identity $MatchingADUsers[0].objectGUID -EmployeeID $currentEmployeeID -ErrorVariable SetError
                    # Check if it changed the user with no errors
                    if ($SetError.Count -eq 0 -or $null -eq $SetError) { break }
                    # If it did complete with an error, and it's the last AD server to try, then log an error
                    elseif ($ADServer -eq $ADServers[$ADServers.Count - 1]) {
                        Write-Host -ForegroundColor Red "Error setting user EmployeeID $firstName $lastName -- $currentEmployeeID"
                        Add-Log -LogType "Error" -DBEmployeeID $currentEmployeeID -ADObjectGUID $MatchingADUsers[0].objectGUID -Details "Error setting the users EmployeeID"
                    }
                }
            }
        }
        # If there are no matches in AD, then skip the user while logging an error
        elseif ($MatchingADUsers.Count -gt 1) {
            Write-Host -ForegroundColor Yellow "Multiple AD user accounts matched for this employee's name. Changes will need to be performed manually."
            Add-Log -LogType "Error" -DBEmployeeID $currentEmployeeID -Details "Multiple AD user accounts matched for this employee's name. Changes will need to be performed manually."
        }
    
    }

}


Write-Host -ForegroundColor Green "Script complete"
Add-Log -LogType "Info" -Details "Script complete"
