# Update Employee ID

THIS SCRIPT HAS BEEN APPROVED FOR RELEASE ON MY PERSONAL PUBLIC FACING SITE



Powershell script to update employeeID field in AD from a remote SQL database.
Intended to be used on an automated basis.

Some variables may need to be changed in the script itself. Those variables are:
$CsvLogPath
$CsvDelimiter
$ADServers
$DryRun
And the query $SQLDBSizeQuery


Requires credentials to pull data from a remote SQL database. Credentials should be stored in file called DB_Creds.json
Keep in mind that json files may need the escape character backslash (\) before some special characters
Example DB_Creds.json
[{
    "RemoteSQLInstance"     :   "database.example.com",
    "RemoteSQLDatabaseName"    :   "MyDatabase",
    "RemoteSQLUser"     :    "username",
    "RemoteSQLPassword" :      "password"
}]
